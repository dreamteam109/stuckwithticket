import pool from "./pool";

export const getAllCategories = async () => {
  const { rows } = await pool.query(`
    select
        id,
        name
    from stuckwithtickets.categories
    where is_deleted = false
  `);
  return rows;
};
