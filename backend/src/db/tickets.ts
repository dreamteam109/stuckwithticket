import { CategoryId } from "../Categories/Categories.types";
import pool from "./pool";
import { complement, isNil, prop } from "ramda";
import { UserId } from "../types";

export const searchTickets = async (
  lastUpdateTimestamp?: number,
  text?: string,
  categories?: CategoryId[],
  priceLimit?: number,
  amount?: number,
  startTimestamp?: number,
  endTimestamp?: number
) => {
  const params = [
    text,
    categories,
    priceLimit,
    amount,
    startTimestamp,
    endTimestamp,
    lastUpdateTimestamp,
  ]
    .map((x, i) => ({ value: x, index: i + 1 }))
    .filter((x) => isNotNil(x.value));

  const $ = (index) => params.findIndex((x) => x.index === index) + 1;

  const textualSearch = `and (t.event_name LIKE '%' || $${$(
    1
  )} || '%' or t.description LIKE '%' || $${$(1)} || '%')`;
  const categoriesSearch = `and t.category_id = ANY($${$(2)}::int[])`;
  const priceSearch = `and t.price <= $${$(3)}`;
  const amountSearch = `and t.amount >= $${$(4)}`;
  const startTimestampSearch = `and t.event_datetime > to_timestamp($${$(
    5
  )} / 1000.0)`;
  const endTimestampSearch = `and t.event_datetime < to_timestamp($${$(
    6
  )} / 1000.0)`;
  const lastUpdateTimestampSearch = `and t.last_update_date > to_timestamp($${$(
    7
  )} / 1000.0)`;

  const { rows } = await pool.query(
    `
    select
        t.id,
        t.user_id as "userId",
        t.event_name as "eventName",
        t.category_id as "categoryId",
        c.name as "categoryName",
        t.description,
        t.event_datetime as "eventDatetime",
        t.price,
        t.amount,
        t.image_id as "imageId",
        t.x,
        t.y,
        t.is_deleted as "isDeleted",
        t.last_update_date as "lastUpdateDate"
    from stuckwithtickets.tickets t
      left join stuckwithtickets.categories c on c.id = t.category_id
    where t.event_datetime > now()
      and t.amount > 0
        ${isNotNil(text) ? textualSearch : ""}
        ${isNotNil(categories) ? categoriesSearch : ""}
        ${isNotNil(priceLimit) ? priceSearch : ""}
        ${isNotNil(amount) ? amountSearch : ""}
        ${isNotNil(startTimestamp) ? startTimestampSearch : ""}
        ${isNotNil(endTimestamp) ? endTimestampSearch : ""}
        ${isNotNil(lastUpdateTimestamp) ? lastUpdateTimestampSearch : ""}
  `,
    params.map(prop<any>("value"))
  );

  return rows;
};

const isNotNil = complement(isNil);

export const sellTicket = async (
  userId: UserId,
  title: string,
  category: number,
  description: string,
  eventTimestamp: number,
  price: number,
  amount: number,
  imageId: string,
  x: number,
  y: number
): Promise<void> => {
  await pool.query(
    `insert into stuckwithtickets.tickets (
    user_id, event_name, category_id, description, event_datetime, price, amount, image_id, x, y)
  values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`,
    [userId, title, category, description, new Date(eventTimestamp), price, amount, imageId, x, y]
  );
};

export const getTicketsByUserId = async (
  userId: number
) => {

  const { rows } = await pool.query(
    `
    select
        t.id,
        t.user_id as "userId",
        t.event_name as "eventName",
        t.category_id as "categoryId",
        c.name as "categoryName",
        t.description,
        t.event_datetime as "eventDatetime",
        t.price,
        t.amount,
        t.image_id as "imageId",
        t.x,
        t.y,
        t.is_deleted as "isDeleted",
        t.last_update_date as "lastUpdateDate"
    from stuckwithtickets.tickets t
      left join stuckwithtickets.categories c on c.id = t.category_id
    where t.is_deleted = false
      and t.user_id = $1
  `,
    [userId]
  );

  return rows;
};

export const getTicketById = async (
  ticketId: number
) => {

  const { rows } = await pool.query(
    `
    select
        t.id,
        t.user_id as "userId",
        t.event_name as "eventName",
        t.category_id as "categoryId",
        c.name as "categoryName",
        t.description,
        t.event_datetime as "eventDatetime",
        t.price,
        t.amount,
        t.image_id as "imageId",
        t.x,
        t.y,
        t.last_update_date as "lastUpdateDate"
    from stuckwithtickets.tickets t
      left join stuckwithtickets.categories c on c.id = t.category_id
    where t.is_deleted = false
      and t.id = $1
  `,
    [ticketId]
  );

  return rows[0];
};

export const editTicket = async (
  ticketId:number,
  title: string,
  categoryId: number,
  description: string,
  eventTimestamp: number,
  price: number,
  amount: number,
  imageId: string,
  x: number,
  y: number
  ): Promise<void> => {
    await pool.query(
     ` update stuckwithtickets.tickets 
      set event_name = $1,
          category_id = $2,
          description = $3,
          event_datetime = $4,
          price = $5,
          amount = $6,
          image_id = $7,
          x = $8,
          y = $9,
          last_update_date = now()
         where id = $10 and is_deleted = false
  `
    ,[title, categoryId, description, new Date(eventTimestamp), price, amount, imageId, x, y, ticketId]);
}

export const logicalDeleteTicket = async (ticketId: number): Promise<void> => {
  await pool.query(
    `
      update stuckwithtickets.tickets
      set is_deleted = true,
          last_update_date = now()
      where id = $1 and is_deleted = false
    `, [ticketId]
  );
}
