import pool from "./pool";
import { ImageRecord } from "./types";

export const uploadImage = async ({id, mimeType, imageData}: ImageRecord): Promise<void> => {
  await pool.query(`
      insert into stuckwithtickets.images (id, mime_type, image_data)
      values ($1, $2, $3)
    `,
    [id, mimeType, imageData]
  );
};

export const getImageById = async (id: string): Promise<ImageRecord> => {
  const { rows }: { rows: ImageRecord[] } = await pool.query(`
    select
      id,
      mime_type as "mimeType",
      image_data as "imageData"
    from stuckwithtickets.images
    where id = $1
  `, [id]);
  return rows[0];
};
