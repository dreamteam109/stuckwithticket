import passport from "passport";
import passportLocal from "passport-local";
import passportJwt from "passport-jwt";
import bcrypt from "bcryptjs";
import { getUserByEmailForAuth } from "../db/users";
import { NextFunction, Request, Response } from "express";

const LocalStrategy = passportLocal.Strategy;
const JWTstrategy = passportJwt.Strategy;
const ExtractJWT = passportJwt.ExtractJwt;

export const configurePassport = () => {
  passport.use(
    "login",
    new LocalStrategy(
      {
        usernameField: "email",
        passwordField: "password",
      },
      async (email, password, done) => {
        try {
          const user = await getUserByEmailForAuth(email);

          const isCorrectPassword = await bcrypt.compare(
            password,
            user?.password ?? ""
          );

          if (!isCorrectPassword) {
            return done(null, false, { message: "Wrong username or password" });
          }

          return done(null, user, { message: "Logged in Successfully" });
        } catch (error) {
          return done(error);
        }
      }
    )
  );

  passport.use(
    new JWTstrategy(
      {
        secretOrKey: process.env.JWT_SECRET,
        jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
      },
      async (token, done) => {
        try {
          const user = await getUserByEmailForAuth(token.user.email);

          return done(null, user ?? false);
        } catch (error) {
          done(error);
        }
      }
    )
  );
};

export const authMiddleware = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  passport.authenticate("jwt", { session: false })(req, res, next);
};
