import { Request, Response, NextFunction } from "express";
import { isEditUserDetailsRequestBody, isLoginRequestBody, isRegisterRequestBody, isUser } from "./Users.typeguards";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";
import passport from "passport";
import { createUser, editUserDetails, getContactInfoById, getUserByEmailForAuth } from "../db/users";
import { getImageById } from "../db/images";
import { UserForEdit } from "./Users.types";

export const registerHandler = async (req: Request, res: Response, next: NextFunction) => {
    if (isRegisterRequestBody(req.body)) {
        const { email, password, firstName, lastName, phoneNumber, imageId } = req.body;

        try {
            const existingUserWithEmail = await getUserByEmailForAuth(email);

            if (existingUserWithEmail) {
                return res.status(409).send("The given email is already registered");
            }

            const hashedPassword = await bcrypt.hash(password, 10);

            await createUser({
                email,
                password: hashedPassword,
                firstName,
                lastName,
                phoneNumber,
                imageId
            });

            res.sendStatus(201);
        } catch (error) {
            next(error);
        }

    } else {
        res.status(400).send("invalid or missing registration data");
    }
}

export const loginHandler = async (req: Request, res: Response, next: NextFunction) => {
    if (isLoginRequestBody(req.body)) {
        passport.authenticate(
            'login',
            { session: false },
            async (error, user, info) => {
                try {
                    if (error) {
                        return next(error);
                    }

                    if (!user) {
                        return res.status(401).send(info.message);
                    }

                    req.login(
                        user,
                        { session: false },
                        async (error) => {
                            if (error) return next(error);

                            const body = { id: user.id, email: user.email };
                            const token = jwt.sign({ user: body }, process.env.JWT_SECRET);

                            return res.json({ token });
                        }
                    );
                } catch (error) {
                    return next(error);
                }
            }
        )(req, res, next);
    } else {
        res.status(400).send("invalid or missing email or password");
    }
}

export const editUserDetailsHandler = async (req: Request, res: Response) => {
    if (isEditUserDetailsRequestBody(req.body) && isUser(req.user)) {
        const { firstName, lastName, phoneNumber, imageId } = req.body;
        const userForEdit: UserForEdit = {
            id: req.user.id,
            firstName,
            lastName,
            phoneNumber,
            imageId
        };
        await editUserDetails(userForEdit);
        res.send();
    } else {
        res.status(400).send("invalid or missing user data on edit");
    }
}

export const getMyUserDetailsHandler = async (req: Request, res: Response) => {
    if (isUser(req.user)) {
        res.send(req.user)
    } else {
        res.status(400).send("invalid or missing user data");
    }
}

export const contactInfoHandler = async (req: Request, res: Response) => {
    // TODO: maybe validate...
    const userId = Number(req.params.id);
    const contactInfo = await getContactInfoById(userId);
    res.json(contactInfo);
}
