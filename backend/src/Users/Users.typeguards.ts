import { isEmail, isNonEmptyString, isPassword, isPhoneNumber, isString } from "../typeguards";
import { EditUserDetailsRequestBody, LoginRequestBody, RegisterRequestBody, User } from "./Users.types";

export const isLoginRequestBody = (body: any): body is LoginRequestBody => isEmail(body?.email) && isPassword(body?.password);

export const isRegisterRequestBody = (body: any): body is RegisterRequestBody =>
    isPassword(body?.password)
    && isEditUserDetailsRequestBody(body);

export const isEditUserDetailsRequestBody = (body: any): body is EditUserDetailsRequestBody =>
    isPhoneNumber(body?.phoneNumber)
    && isNonEmptyString(body?.firstName)
    && isNonEmptyString(body?.lastName)

export const isUser = (obj: any): obj is User =>
    isNonEmptyString(obj?.firstName)
    && isNonEmptyString(obj?.lastName)
    && isEmail(obj?.email)
    && isNonEmptyString(obj?.password)
    && isPhoneNumber(obj?.phoneNumber)
