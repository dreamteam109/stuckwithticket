import { Email, Password, PhoneNumber } from "./types";

export const isEmail = (obj: any): obj is Email => typeof obj === "string" && obj.includes("@"); // TODO consider different validation
export const isPassword = (obj: any): obj is Password => isNonEmptyString(obj); // TODO consider different validation
export const isPhoneNumber = (obj: any): obj is PhoneNumber => isNonEmptyString(obj); // TODO consider different validation
export const isNonEmptyString = (obj: any): obj is string => isString(obj) && obj.length > 0;
export const isString = (obj: any): obj is string => typeof obj === "string";
export const isNumber = (obj: any): obj is number => typeof obj === "number";
export const isPositiveNumber = (obj: any): obj is number => isNumber(obj) && obj > 0;