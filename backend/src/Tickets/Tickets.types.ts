import { CategoryId } from "../Categories/Categories.types";
import { NonEmptyString } from "../types";

export interface SellTicketRequestBody {
    title: NonEmptyString;
    category: CategoryId;
    description: string;
    eventTimestamp: number;
    price: number;
    amount: number;
    imageId?: string;
    x: number;
    y: number;
}

export interface EditTicketRequestBody extends SellTicketRequestBody {
    id: number
}

export interface SearchTicketsRequestBody {
    lastUpdateTimestamp?: number;
    text?: string;
    startTimestamp?: number;
    endTimestamp?: number;
    categories?: CategoryId[];
    priceLimit?: number;
    amount?: number;
    // TODO city/location
}
