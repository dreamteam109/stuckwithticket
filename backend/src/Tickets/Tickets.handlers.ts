import { Request, Response } from "express";
import { searchTickets, sellTicket, getTicketsByUserId, getTicketById, editTicket, logicalDeleteTicket } from "../db/tickets";
import { isUser } from "../Users/Users.typeguards";
import { isEditTicketRequestBody, isSearchTicketsRequestBody, isSellTicketRequestBody } from "./Tickets.typeguards";

export const sellTicketHandler = async (req: Request, res: Response) => {
    if (isSellTicketRequestBody(req.body) && isUser(req.user)) {
        const { amount, category, description, eventTimestamp, price, title, imageId, x, y } = req.body;
        await sellTicket(req.user.id, title, category, description, eventTimestamp, price, amount, imageId, x, y);
        res.send();
    } else {
        res.status(400).send("invalid or missing ticket data");
    }
}

export const editTicketHandler = async (req: Request, res: Response) => {
    if (isEditTicketRequestBody(req.body)) {
        const { amount, category, description, eventTimestamp, price, title, imageId, x, y } = req.body;
         await editTicket(Number(req.params.id), title, category, description, eventTimestamp, price, amount, imageId, x, y); 
         res.send();
    } else {
        res.status(400).send("invalid or missing ticket data");
    }
}

export const deleteTicketHandler = async (req: Request, res: Response) => {
    if (isUser(req.user)) {
        const ticketId = Number(req.params.id);
        const ticket = await getTicketById(ticketId);
        
        if (!ticket || ticket.userId !== req.user.id) {
            return res.sendStatus(400);
        } else {
            await logicalDeleteTicket(ticketId);
            return res.send();
        }
    } else {
        res.status(400).send("invalid or missing user data");
    }
}

export const searchTicketsHandler = async (req: Request, res: Response) => {
    if (isSearchTicketsRequestBody(req.body)) {
        const { lastUpdateTimestamp, categories, endTimestamp, priceLimit, startTimestamp, text, amount } = req.body;
        // NOTE: WIP Implementation
        const tickets = await searchTickets(lastUpdateTimestamp, text, categories, priceLimit, amount, startTimestamp, endTimestamp);
        res.json(tickets);
    } else {
        res.status(400).send("invalid or missing search data");
    }
}

export const getMyTicketsHandler = async (req: Request, res: Response) => {
    if (isUser(req.user)) {
        const tickets = await getTicketsByUserId(req.user.id);
        res.json(tickets);
    } else {
        res.status(400).send("invalid or missing user data");
    }
}

export const getTicketDetails = async (req: Request, res: Response) => {
    const ticket = await getTicketById(Number(req.params.id));
    res.json(ticket);
}
