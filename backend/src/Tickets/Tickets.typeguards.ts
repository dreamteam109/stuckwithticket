import { isNonEmptyString, isPositiveNumber, isNumber } from "../typeguards";
import {
  EditTicketRequestBody,
  SearchTicketsRequestBody,
  SellTicketRequestBody,
} from "./Tickets.types";

export const isSellTicketRequestBody = (
  body: any
): body is SellTicketRequestBody => {

return isPositiveNumber(body?.category) && // TODO if categories are static, maybe query them in advance on server initialization and validate this is one of them?
    isNonEmptyString(body?.title) &&
    isNonEmptyString(body?.description) && // TODO is it really required to be non empty?
    isNumber(body?.eventTimestamp) &&
    isPositiveNumber(body?.price) &&
    isPositiveNumber(body?.amount) && 
    isNumber(body.x) &&
    isNumber(body.y);

};

export const isEditTicketRequestBody = (
  body: any
): body is EditTicketRequestBody => isSellTicketRequestBody(body);

// TODO maybe offer a better solution?
export const isSearchTicketsRequestBody = (
  body: any
): body is SearchTicketsRequestBody => true;
