import { Router } from "express"
import { authMiddleware } from "../auth/auth";
import { deleteTicketHandler, editTicketHandler, getMyTicketsHandler, getTicketDetails, searchTicketsHandler, sellTicketHandler } from "./Tickets.handlers";

export const createTicketsRouter = () => {
    const router = Router();
    router.post('/sell', authMiddleware, sellTicketHandler);
    router.post('/edit/:id', authMiddleware, editTicketHandler);
    router.delete('/:id', authMiddleware, deleteTicketHandler);
    router.post('/search', searchTicketsHandler);
    router.get('/me', authMiddleware, getMyTicketsHandler);
    router.get('/:id', getTicketDetails);

    return router;
}
