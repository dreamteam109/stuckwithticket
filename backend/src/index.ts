import dotenv from 'dotenv';
dotenv.config();

import express from 'express';
import passport from 'passport';
import  { configurePassport } from "./auth/auth";
import bodyParser from 'body-parser';
import morgan from 'morgan';
import { createCategoriesRouter } from './Categories/Categories.router';
import { createImagesRouter } from './Images/Images.router';
import { createUsersRouter } from './Users/Users.router';
import { createTicketsRouter } from './Tickets/Tickets.router';

const app = express();
const port = 8080;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(passport.initialize()); // NOTE: not sure if this is necessary
configurePassport();
app.use(morgan('combined'));

const start = async () => {
    app.use('/categories', createCategoriesRouter());
    app.use('/images', createImagesRouter());
    app.use('/users', createUsersRouter());
    app.use('/tickets', createTicketsRouter());

    app.listen(port, () => console.log(`server is listening on ${port}`));
}

start();
