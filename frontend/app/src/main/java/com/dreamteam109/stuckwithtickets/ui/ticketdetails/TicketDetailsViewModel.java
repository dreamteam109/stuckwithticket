package com.dreamteam109.stuckwithtickets.ui.ticketdetails;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.dreamteam109.stuckwithtickets.model.Model;
import com.dreamteam109.stuckwithtickets.model.models.Ticket;

public class TicketDetailsViewModel extends AndroidViewModel {
    private int ticketId;
    private LiveData<Ticket> ticket;

    public TicketDetailsViewModel(@NonNull Application application, int ticketId) {
        super(application);
        this.ticketId = ticketId;
        refreshTicket();
    }

    public void refreshTicket() {
        ticket = Model.instance.getTicketById(ticketId);
    }

    public LiveData<Ticket> getTicket() {
        return ticket;
    }

    public static class Factory implements ViewModelProvider.Factory {

        @NonNull
        private final Application mApplication;

        private final int mTicketId;

        public Factory(@NonNull Application application, int ticketId) {
            mApplication = application;
            mTicketId = ticketId;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> aClass) {
            return (T) new TicketDetailsViewModel(mApplication, mTicketId);
        }
    }
}