package com.dreamteam109.stuckwithtickets.model.restapi.apis;

import com.dreamteam109.stuckwithtickets.model.models.Category;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface CategoriesApi {
    @GET("categories")
    Call<List<Category>> getCategories();
}
