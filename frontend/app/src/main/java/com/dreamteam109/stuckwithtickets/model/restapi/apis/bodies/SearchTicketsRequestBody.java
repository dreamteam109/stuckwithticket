package com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies;

import java.util.List;

public class SearchTicketsRequestBody {
    final Long lastUpdateTimestamp;
    final String text;
    final List<Integer> categories;
    final Long startTimestamp;
    final Long endTimestamp;
    final Integer priceLimit;
    final Integer amount;

    public SearchTicketsRequestBody(Long lastUpdateTimestamp, String text, List<Integer> categories, Long startTimestamp, Long endTimestamp, Integer priceLimit, Integer amount) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
        this.text = text;
        this.categories = categories;
        this.startTimestamp = startTimestamp;
        this.endTimestamp = endTimestamp;
        this.priceLimit = priceLimit;
        this.amount = amount;
    }
}
