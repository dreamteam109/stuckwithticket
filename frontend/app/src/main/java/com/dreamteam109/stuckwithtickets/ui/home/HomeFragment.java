package com.dreamteam109.stuckwithtickets.ui.home;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.bumptech.glide.Glide;
import com.dreamteam109.stuckwithtickets.MainActivity;
import com.dreamteam109.stuckwithtickets.R;
import com.dreamteam109.stuckwithtickets.StuckWithTicketsApplication;
import com.dreamteam109.stuckwithtickets.databinding.FragmentHomeBinding;
import com.dreamteam109.stuckwithtickets.model.Model;
import com.dreamteam109.stuckwithtickets.model.models.Ticket;

import java.util.List;

public class HomeFragment extends Fragment {

    private static final String LOGO_CLICK_LISTENER_NAME = "HomeScrollUp";

    private HomeViewModel homeViewModel;
    private FragmentHomeBinding binding;
    private HomeTicketsListAdapter adapter;
    private RecyclerView recyclerView;
    private EditText searchBar;
    private ImageButton filterButton;
    private ImageButton mapButton;
    TextView homeNoneTicketsCommentTv;
    SwipeRefreshLayout swipeRefresh;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);

        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        swipeRefresh = root.findViewById(R.id.home_ticketlist_swiperefresh);
        swipeRefresh.setOnRefreshListener(() -> Model.instance.refreshTicketsList());

        recyclerView = root.findViewById(R.id.home_ticketlist_rv);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter = new HomeTicketsListAdapter();
        recyclerView.setAdapter(adapter);

        filterButton = root.findViewById(R.id.home_filter_btn);

        filterButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_nav_home_to_filterFragment);
            }
        });

        mapButton = root.findViewById(R.id.home_map_btn);
        mapButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(v).navigate(R.id.action_nav_home_to_mapFragment);
            }
        });


        searchBar = (EditText) root.findViewById(R.id.home_search_bar_et);

        searchBar.addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                homeViewModel.setSearchString(searchBar.getText().toString());
            }
        }));


        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                int ticketId = homeViewModel.getTicketsList().getValue().get(position).getId();
                Log.d("TAG", "Ticket id " + ticketId + " clicked.");
                Navigation.findNavController(v).navigate(HomeFragmentDirections.actionNavHomeToTicketDetailsFragment(ticketId));
            }
        });

        homeViewModel.getTicketsList().observe(getViewLifecycleOwner(), new Observer<List<Ticket>>() {

            @Override
            public void onChanged(List<Ticket> tickets) {
                refresh();
            }
        });
        homeViewModel.getTicketsList().observe(getViewLifecycleOwner(), list1 -> refresh());

        swipeRefresh.setRefreshing(Model.instance.getTicketListLoadingState().getValue() == Model.TicketListLoadingState.loading);
        Model.instance.getTicketListLoadingState().observe(getViewLifecycleOwner(), ticketListLoadingState -> {
            if (ticketListLoadingState == Model.TicketListLoadingState.loading) {
                swipeRefresh.setRefreshing(true);
            } else {
                swipeRefresh.setRefreshing(false);
            }

        });

        homeNoneTicketsCommentTv = root.findViewById(R.id.home_none_tickets_comment_tv);

        refreshComment();
        return root;
    }


    @Override
    public void onStart() {
        super.onStart();

        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity != null) {
            mainActivity.addLogoOnClickListener(LOGO_CLICK_LISTENER_NAME, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("StuckWithTicketsLog", "HomeFragment - logo click listener");
                    recyclerView.smoothScrollToPosition(0);
                }
            });
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity != null) {
            mainActivity.removeLogoOnClickListener(LOGO_CLICK_LISTENER_NAME);
        }
    }

    private void refresh() {
        adapter.notifyDataSetChanged();
        refreshComment();
    }

    private void refreshComment() {
        Log.d("TAG", "HomeFragment - list content: " + homeViewModel.getTicketsList().getValue());
        if (homeViewModel.getTicketsList().getValue() == null) {
            homeNoneTicketsCommentTv.setVisibility(View.VISIBLE);
            Log.d("TAG", "HomeFragment - comment visible");
        } else if (homeViewModel.getTicketsList().getValue() != null && homeViewModel.getTicketsList().getValue().size() == 0) {
            homeNoneTicketsCommentTv.setVisibility(View.VISIBLE);
            Log.d("TAG", "HomeFragment - comment visible");
        } else {
            homeNoneTicketsCommentTv.setVisibility(View.GONE);
            Log.d("TAG", "HomeFragment - comment invisible");
        }
    }

    interface OnItemClickListener {
        void onItemClick(View v, int position);
    }

    class HomeTicketViewHolder extends RecyclerView.ViewHolder {

        TextView titleTv;
        TextView categoryTv;
        ImageView imageImv;
        TextView dateTv;
        TextView priceTv;
        TextView amountTv;
        TextView noDataCommentTv;


        public HomeTicketViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);

            titleTv = itemView.findViewById(R.id.homelistitem_title_tv);
            categoryTv = itemView.findViewById(R.id.homelistitem_category_tv);
            imageImv = itemView.findViewById(R.id.homelistitem_image_imv);
            dateTv = itemView.findViewById(R.id.homelistitem_date_tv);
            priceTv = itemView.findViewById(R.id.homelistitem_price_tv);
            amountTv = itemView.findViewById(R.id.homelistitem_amount_tv);
            noDataCommentTv = itemView.findViewById(R.id.home_none_tickets_comment_tv);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();
                    listener.onItemClick(v, pos);
                }
            });
        }
    }

    class HomeTicketsListAdapter extends RecyclerView.Adapter<HomeTicketViewHolder> {
        OnItemClickListener listener;

        public void setOnItemClickListener(OnItemClickListener listener) {
            this.listener = listener;
        }

        @NonNull
        @Override
        public HomeTicketViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.home_ticket_list_item, parent, false);
            HomeTicketViewHolder holder = new HomeTicketViewHolder(view, listener);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull HomeTicketViewHolder holder, int position) {
            Ticket ticket = homeViewModel.getTicketsList().getValue().get(position);

            holder.titleTv.setText(ticket.getEventName());
            holder.categoryTv.setText(ticket.getCategoryName());
            Glide.with(HomeFragment.this)
                    .load(Model.instance.getFullImageUrl(ticket.getImageId()))
                    .fitCenter()
                    .placeholder(R.drawable.event_image_placeholder)
                    .fallback(R.drawable.event_image_placeholder)
                    .into(holder.imageImv);
            holder.dateTv.setText(Model.instance.dateTimeFormatter.format(ticket.getEventDateTime()));
            holder.priceTv.setText("$" + Integer.toString(ticket.getPrice()));
            holder.amountTv.setText(Integer.toString(ticket.getAmount()));
        }

        @Override
        public int getItemCount() {
            if (homeViewModel.getTicketsList().getValue() == null) {
                return 0;
            }
            return homeViewModel.getTicketsList().getValue().size();
        }
    }


}