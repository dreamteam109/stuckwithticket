package com.dreamteam109.stuckwithtickets.ui.yourtickets;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.AlertDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.dreamteam109.stuckwithtickets.R;
import com.dreamteam109.stuckwithtickets.databinding.FragmentYourTicketsBinding;
import com.dreamteam109.stuckwithtickets.model.Model;
import com.dreamteam109.stuckwithtickets.model.models.Ticket;

import java.util.List;

public class YourTicketsFragment extends Fragment {

    private YourTicketsViewModel yourTicketsViewModel;
    private FragmentYourTicketsBinding binding;
    private YourTicketsListAdapter adapter;
    private RecyclerView recyclerView;
    TextView noneTicketsCommentTv;
    SwipeRefreshLayout swipeRefresh;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        yourTicketsViewModel = new ViewModelProvider(this).get(YourTicketsViewModel.class);

        binding = FragmentYourTicketsBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        swipeRefresh = root.findViewById(R.id.your_tickets_swiperefresh);
        swipeRefresh.setOnRefreshListener(() -> Model.instance.refreshTicketsList());

        noneTicketsCommentTv = root.findViewById(R.id.your_tickets_none_tickets_comment_tv);

        recyclerView = root.findViewById(R.id.your_tickets_rv);
        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter = new YourTicketsListAdapter();
        recyclerView.setAdapter(adapter);


        adapter.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(View v, int position) {
                int ticketId = yourTicketsViewModel.getTicketsList().getValue().get(position).getId();
                Log.d("StuckWithTicketsLog", "YourTicketsFragment - Ticket id " + ticketId + " clicked.");
                Navigation.findNavController(v).navigate(YourTicketsFragmentDirections.actionYourTicketsFragmentToNavTicketDetailsFragment(ticketId));
            }

            @Override
            public void onEditClick(View view, int position) {
                int ticketId = yourTicketsViewModel.getTicketsList().getValue().get(position).getId();
                Log.d("StuckWithTicketsLog", "YourTicketsFragment - Ticket id " + ticketId + " clicked to edit.");
                Navigation.findNavController(view).navigate(YourTicketsFragmentDirections.actionYourTicketsFragmentToEditTicketFragment(ticketId));
            }

            @Override
            public void onDeleteClick(View view, int position) {
                int ticketId = yourTicketsViewModel.getTicketsList().getValue().get(position).getId();
                Log.d("StuckWithTicketsLog", "YourTicketsFragment - Ticket id " + ticketId + " clicked to delete (opening yes/no dialog).");
                new AlertDialog.Builder(getContext(), R.style.AlertDialogTheme)
                        .setMessage(R.string.delete_ticket_dialog)
                        .setPositiveButton(R.string.yes, (dialog, which) -> {
                            Model.instance.deleteTicket(ticketId, () -> {
                                Toast.makeText(getContext(), R.string.ticket_deleted_dialog , Toast.LENGTH_LONG).show();
                            });
                        })
                        .setNegativeButton(R.string.no, (dialog, which) -> {
                        })
                        .show();
            }
        });

        yourTicketsViewModel.getTicketsList().observe(getViewLifecycleOwner(), new Observer<List<Ticket>>() {
            @Override
            public void onChanged(List<Ticket> tickets) {
                refresh();
            }
        });

        refreshComment();

        swipeRefresh.setRefreshing(Model.instance.getTicketListLoadingState().getValue() == Model.TicketListLoadingState.loading);
        Model.instance.getTicketListLoadingState().observe(getViewLifecycleOwner(), ticketListLoadingState -> {
            if (ticketListLoadingState == Model.TicketListLoadingState.loading) {
                swipeRefresh.setRefreshing(true);
            } else {
                swipeRefresh.setRefreshing(false);
            }

        });

        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void refresh() {
        adapter.notifyDataSetChanged();
        refreshComment();
    }

    private void refreshComment() {
        Log.d("TAG", "YourTicketsFragment -  list content: " + yourTicketsViewModel.getTicketsList().getValue());
        if (yourTicketsViewModel.getTicketsList().getValue() == null) {
            noneTicketsCommentTv.setVisibility(View.VISIBLE);
            Log.d("TAG", "YourTicketsFragment - comment visible");
        } else if (yourTicketsViewModel.getTicketsList().getValue() != null && yourTicketsViewModel.getTicketsList().getValue().size() == 0) {
            noneTicketsCommentTv.setVisibility(View.VISIBLE);
            Log.d("TAG", "YourTicketsFragment - comment visible");
        } else {
            noneTicketsCommentTv.setVisibility(View.GONE);
            Log.d("TAG", "YourTicketsFragment - comment invisible");
        }
    }

    interface OnItemClickListener {
        void onItemClick(View view, int position);

        void onEditClick(View view, int position);

        void onDeleteClick(View view, int position);
    }

    class YourTicketViewHolder extends RecyclerView.ViewHolder {

        TextView titleTv;
        TextView dateTv;
        TextView priceTv;
        TextView noDataCommentTv;
        ImageButton editImgbtn;
        ImageButton deleteImgbtn;

        public YourTicketViewHolder(@NonNull View itemView, OnItemClickListener listener) {
            super(itemView);

            titleTv = itemView.findViewById(R.id.yourticketslistitem_title_tv);
            dateTv = itemView.findViewById(R.id.yourticketslistitem_date_tv);
            priceTv = itemView.findViewById(R.id.yourticketslistitem_price_tv);
            noDataCommentTv = itemView.findViewById(R.id.your_tickets_none_tickets_comment_tv);
            editImgbtn = itemView.findViewById(R.id.yourticketslistitem_edit_imbtn);
            deleteImgbtn = itemView.findViewById(R.id.yourticketslistitem_delete_imbtn);

            editImgbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    listener.onEditClick(view, pos);
                }
            });

            deleteImgbtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    listener.onDeleteClick(view, pos);
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = getAdapterPosition();
                    listener.onItemClick(view, pos);
                }
            });

        }
    }


    class YourTicketsListAdapter extends RecyclerView.Adapter<YourTicketViewHolder> {
        OnItemClickListener listener;

        public void setOnItemClickListener(OnItemClickListener listener) {
            this.listener = listener;
        }

        @NonNull
        @Override
        public YourTicketViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = getLayoutInflater().inflate(R.layout.your_ticket_list_item, parent, false);
            YourTicketViewHolder holder = new YourTicketViewHolder(view, listener);
            return holder;
        }

        @Override
        public void onBindViewHolder(@NonNull YourTicketViewHolder holder, int position) {
            Ticket ticket = yourTicketsViewModel.getTicketsList().getValue().get(position);

            holder.titleTv.setText(ticket.getEventName());
            holder.dateTv.setText(Model.instance.dateFormatter.format(ticket.getEventDateTime()));
            holder.priceTv.setText("$" + Integer.toString(ticket.getPrice()));

        }

        @Override
        public int getItemCount() {
            if (yourTicketsViewModel.getTicketsList().getValue() == null) {
                return 0;
            }
            return yourTicketsViewModel.getTicketsList().getValue().size();
        }

    }
}