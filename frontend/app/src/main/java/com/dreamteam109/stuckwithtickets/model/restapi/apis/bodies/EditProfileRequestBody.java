package com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies;

public class EditProfileRequestBody {
    final String firstName;
    final String lastName;
    final String phoneNumber;
    final String imageId;

    public EditProfileRequestBody(String firstName, String lastName, String phoneNumber, String imageId) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.imageId = imageId;
    }
}
