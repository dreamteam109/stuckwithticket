package com.dreamteam109.stuckwithtickets.ui.editticket;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dreamteam109.stuckwithtickets.R;
import com.dreamteam109.stuckwithtickets.StuckWithTicketsApplication;
import com.dreamteam109.stuckwithtickets.databinding.FragmentEditTicketBinding;
import com.dreamteam109.stuckwithtickets.model.models.Category;
import com.dreamteam109.stuckwithtickets.model.Model;
import com.dreamteam109.stuckwithtickets.model.models.Ticket;
import com.dreamteam109.stuckwithtickets.model.Validators;
import com.google.android.material.textfield.TextInputLayout;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class EditTicketFragment extends Fragment {
    private static final int REQUEST_CAMERA = 1;
    private static final int PICK_IMAGE = 2;

    private EditTicketViewModel editTicketViewModel;
    private FragmentEditTicketBinding binding;
    private List<Category> allCategories = new ArrayList<Category>();

    ProgressBar progressBar;
    TextInputLayout titleTil;
    Spinner categorySp;
    TextInputLayout descriptionTil;
    CalendarView dateCv;
    TimePicker timeTp;
    TextInputLayout amountTil;
    TextInputLayout priceTil;
    ImageView imageImv;
    Button cameraBtn;
    Button clearImageBtn;
    Button galleryBtn;
    TextInputLayout locationTil;
    Button saveBtn;
    Button cancelBtn;

    ArrayAdapter<String> categoriesAdapter;
    Geocoder geocoder = new Geocoder(StuckWithTicketsApplication.getContext(), Locale.getDefault());

    int ticketId;
    Ticket ticket;
    String locationAddressBeforeEdit;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = FragmentEditTicketBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        titleTil = root.findViewById(R.id.edit_ticket_title_field_til);
        categorySp = root.findViewById(R.id.edit_ticket_category_field_spinner);
        descriptionTil = root.findViewById(R.id.edit_ticket_description_field_til);
        dateCv = root.findViewById(R.id.edit_ticket_date_field_cv);
        timeTp = root.findViewById(R.id.edit_ticket_time_field_tp);
        amountTil = root.findViewById(R.id.edit_ticket_amount_field_til);
        priceTil = root.findViewById(R.id.edit_ticket_price_field_til);
        imageImv = root.findViewById(R.id.edit_ticket_image_imv);
        cameraBtn = root.findViewById(R.id.edit_ticket_camera_btn);
        clearImageBtn = root.findViewById(R.id.edit_ticket_clear_image_btn);
        galleryBtn = root.findViewById(R.id.edit_ticket_gallery_btn);
        locationTil = root.findViewById(R.id.edit_ticket_location_field_til);
        saveBtn = root.findViewById(R.id.edit_ticket_save_btn);
        cancelBtn = root.findViewById(R.id.edit_ticket_cancel_btn);
        progressBar = root.findViewById(R.id.edit_ticket_progressbar);

        categoriesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
                allCategories.stream().map(category -> category.getName()).collect(Collectors.toList()));
        categorySp.setAdapter(categoriesAdapter);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                saveBtn.setEnabled(false);
                cancelBtn.setEnabled(false);
                cameraBtn.setEnabled(false);
                clearImageBtn.setEnabled(false);
                galleryBtn.setEnabled(false);

                Log.d("StuckWithTicketsLog", "edit ticket save click");
                if (!Validators.isNotEmptyString(editTicketViewModel.getTitle().getValue())) {
                    Toast.makeText(getContext().getApplicationContext(), R.string.title_is_empty, Toast.LENGTH_LONG).show();
                } else if (!Validators.isNotEmptyString(editTicketViewModel.getDescription().getValue())) {
                    Toast.makeText(getContext().getApplicationContext(), R.string.description_is_empty, Toast.LENGTH_LONG).show();
                } else if (!Validators.isNotNull(editTicketViewModel.getAmount().getValue())) {
                    Toast.makeText(getContext().getApplicationContext(), R.string.amount_is_empty, Toast.LENGTH_LONG).show();
                } else if (!Validators.isNotNull(editTicketViewModel.getPrice().getValue())) {
                    Toast.makeText(getContext().getApplicationContext(), R.string.price_is_empty, Toast.LENGTH_LONG).show();
                } else if (!Validators.isNotEmptyString(editTicketViewModel.getLocation().getValue())) {
                    Toast.makeText(getContext().getApplicationContext(), R.string.location_is_empty, Toast.LENGTH_LONG).show();
                } else {
                    // NOTE: This check is a quick patch for fixing the changing x,y each time we save without editing the location
                    if (editTicketViewModel.getLocation().getValue().equals(locationAddressBeforeEdit)) {
                        editTicketViewModel.editTicket(timeTp.getHour(), timeTp.getMinute(), ticket.getLocationPnt().latitude,
                                ticket.getLocationPnt().longitude, new Model.EditTicketListener() {
                                    @Override
                                    public void onComplete() {
                                        Navigation.findNavController(view).navigateUp();
                                    }
                                });
                    } else {
                        try {
                            List<Address> listAdress = geocoder.getFromLocationName(editTicketViewModel.getLocation().getValue(), 1);
                            if (listAdress.size() == 0) {
                                Toast.makeText(getContext().getApplicationContext(), R.string.location_is_invalid, Toast.LENGTH_LONG).show();
                            } else {
                                Address addLocation = listAdress.get(0);
                                editTicketViewModel.editTicket(timeTp.getHour(), timeTp.getMinute(), addLocation.getLatitude(),
                                        addLocation.getLongitude(), new Model.EditTicketListener() {
                                            @Override
                                            public void onComplete() {
                                                Navigation.findNavController(view).navigateUp();
                                            }
                                        });
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        });
        progressBar.setVisibility(View.GONE);
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                Log.d("TAG", "edit ticket cancel click");
                Navigation.findNavController(v).navigateUp();
            }
        });

        titleTil.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTicketViewModel.setTitle(titleTil.getEditText().getText().toString());
            }
        }));

        categorySp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d(" TAG", categorySp.getSelectedItem().toString());
                editTicketViewModel.setCategory(allCategories.stream().
                        filter(category -> category.getName().
                                equals(categorySp.getSelectedItem().toString())).collect(Collectors.toList()).get(0).getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d(" TAG", "category is not clicked");

            }
        });

        descriptionTil.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTicketViewModel.setDescription(descriptionTil.getEditText().getText().toString());
            }
        }));

        dateCv.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                long timestamp = LocalDate.of(year, month + 1, dayOfMonth).atTime(LocalTime.MIN).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
                editTicketViewModel.setDate(timestamp);
            }
        });

        amountTil.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    editTicketViewModel.setAmount(Integer.parseInt(amountTil.getEditText().getText().toString()));
                } catch (Exception e) {
                    editTicketViewModel.setAmount(null);
                }
            }
        }));

        priceTil.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    editTicketViewModel.setPrice(Integer.parseInt(priceTil.getEditText().getText().toString()));
                } catch (Exception e) {
                    editTicketViewModel.setPrice(null);
                }
            }
        }));

        locationTil.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editTicketViewModel.setLocation(locationTil.getEditText().getText().toString());
            }
        }));

        cameraBtn.setOnClickListener(v -> {
            progressBar.setVisibility(View.VISIBLE);
            openSelectImageFromCamera();
            progressBar.setVisibility(View.GONE);
        });

        clearImageBtn.setOnClickListener(v -> {
            progressBar.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
            clearImage();
        });

        galleryBtn.setOnClickListener(v -> {
            progressBar.setVisibility(View.VISIBLE);
            openSelectImageFromGallery();
            progressBar.setVisibility(View.GONE);
        });

        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        ticketId = EditTicketFragmentArgs.fromBundle(getArguments()).getTicketId();
        editTicketViewModel = new ViewModelProvider(this, new EditTicketViewModel.Factory(getActivity().getApplication(), ticketId))
                .get(EditTicketViewModel.class);
        progressBar.setVisibility(View.VISIBLE);

        // get Categories list, and update them when they change
        editTicketViewModel.getCategoriesList().observe(getViewLifecycleOwner(), new Observer<List<Category>>() {
            @Override
            public void onChanged(List<Category> categories) {
                allCategories = categories;

                Log.d("TAG", categories.toString());
                categoriesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
                        allCategories.stream().map(category -> category.getName()).collect(Collectors.toList()));
                categorySp.setAdapter(categoriesAdapter);

                if (ticket != null) {
                    categorySp.setSelection(allCategories.indexOf(
                            allCategories.stream().filter(category -> category.getId() == ticket.getCategoryId()).collect(Collectors.toList()).get(0)
                    ));
                    editTicketViewModel.setCategory(ticket.getCategoryId());
                }
            }
        });

        editTicketViewModel.getTicket().observe(getViewLifecycleOwner(), ticket -> {
            if (ticket != null) {
                EditTicketFragment.this.ticket = ticket;

                titleTil.getEditText().setText(ticket.getEventName());
                editTicketViewModel.setTitle(ticket.getEventName());

                descriptionTil.getEditText().setText(ticket.getDescription());
                editTicketViewModel.setDescription(ticket.getDescription());

                amountTil.getEditText().setText(Integer.toString(ticket.getAmount()));
                editTicketViewModel.setAmount(ticket.getAmount());

                priceTil.getEditText().setText(Integer.toString(ticket.getPrice()));
                editTicketViewModel.setPrice(ticket.getPrice());

                Double latitude = ticket.getLocationPnt().latitude;
                Double longitude = ticket.getLocationPnt().longitude;

                List<Address> address;
                try {
                    address = geocoder.getFromLocation(latitude, longitude, 1);
                    locationAddressBeforeEdit = address.get(0).getAddressLine(0);
                    locationTil.getEditText().setText(locationAddressBeforeEdit);

                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (allCategories.size() > 0) {
                    categorySp.setSelection(allCategories.indexOf(
                            allCategories.stream().filter(category -> category.getId() == ticket.getCategoryId()).collect(Collectors.toList()).get(0)
                    ));
                    editTicketViewModel.setCategory(ticket.getCategoryId());
                }

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(ticket.getEventDateTime());
                dateCv.setDate(calendar.getTimeInMillis());
                timeTp.setHour(calendar.get(Calendar.HOUR_OF_DAY));
                timeTp.setMinute(calendar.get(Calendar.MINUTE));
                editTicketViewModel.setDate(calendar.getTimeInMillis());

                Glide.with(EditTicketFragment.this)
                        .load(Model.instance.getFullImageUrl(ticket.getImageId()))
                        .fitCenter()
                        .placeholder(R.drawable.event_image_placeholder)
                        .fallback(R.drawable.event_image_placeholder)
                        .into(imageImv);

                progressBar.setVisibility(View.GONE);
            }
        });

        editTicketViewModel.getBitmap().observe(getViewLifecycleOwner(), new Observer<Bitmap>() {
            @Override
            public void onChanged(Bitmap bitmap) {
                Glide.with(EditTicketFragment.this)
                        .clear(imageImv);

                if (bitmap == null) {
                    imageImv.setImageResource(R.drawable.event_image_placeholder);
                } else {
                    imageImv.setImageBitmap(bitmap);
                }
            }
        });


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                editTicketViewModel.setBitmap((Bitmap) extras.get("data"));
            }
        } else if (requestCode == PICK_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(StuckWithTicketsApplication.getContext().getContentResolver(), data.getData());
                } catch (IOException e) {
                    Toast.makeText(getContext(), R.string.problem_getting_selected_image, Toast.LENGTH_LONG);
                }
                editTicketViewModel.setBitmap(bitmap);
            }
        }
    }

    private void openSelectImageFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void openSelectImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/png image/jpeg");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    private void clearImage() {
        editTicketViewModel.setBitmap(null);
    }

}