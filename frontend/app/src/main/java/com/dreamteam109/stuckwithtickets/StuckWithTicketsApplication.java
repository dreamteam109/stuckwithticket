package com.dreamteam109.stuckwithtickets;

import android.app.Application;
import android.content.Context;

public class StuckWithTicketsApplication extends Application {

    static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static Context getContext() {
        return context;
    }
}
