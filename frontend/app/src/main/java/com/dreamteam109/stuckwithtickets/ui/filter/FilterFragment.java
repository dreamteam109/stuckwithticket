package com.dreamteam109.stuckwithtickets.ui.filter;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dreamteam109.stuckwithtickets.R;
import com.dreamteam109.stuckwithtickets.databinding.FragmentFilterBinding;
import com.dreamteam109.stuckwithtickets.model.models.Category;
import com.google.android.material.textfield.TextInputLayout;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class FilterFragment extends Fragment {

    private FragmentFilterBinding binding;
    private FilterViewModel filterViewModel;
    private TextInputLayout priceLimit;
    private TextInputLayout amount;
    private List<Category> allCategories;
    private String[] categoriesArray;
    private boolean[] selectedCategories;
    private List<String> pickedCategories;
    private ArrayList<Integer> categoriesList = new ArrayList<>();
    ProgressBar progressBar;

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        filterViewModel = new ViewModelProvider(this).get(FilterViewModel.class);
        binding = FragmentFilterBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        Button cancelButton = root.findViewById(R.id.filter_cancel_btn);
        Button applyButton = root.findViewById(R.id.filter_apply_btn);
        Button resetButton = root.findViewById(R.id.filter_reset_btn);

        priceLimit = root.findViewById(R.id.filter_price_limit);
        amount = root.findViewById(R.id.filter_amount);
        CalendarView fromDate = (CalendarView) root.findViewById(R.id.filter_start_date);
        CalendarView toDate = (CalendarView) root.findViewById(R.id.filter_end_date);
        TextView categories = root.findViewById(R.id.filter_categories);
        progressBar = root.findViewById(R.id.filter_progressbar);

        Long now = new Date().getTime();
        Long nextYear = now + 1000L * 60 * 60 * 24 * 365;
        Long start = Optional.ofNullable(filterViewModel.getStartTimestamp()).orElse(now);
        Long end = Optional.ofNullable(filterViewModel.getEndTimestamp()).orElse(nextYear);

        fromDate.setDate(start);
        toDate.setDate(end);
        filterViewModel.setFromDate(start);
        filterViewModel.setToDate(end);

        Integer minAmount = filterViewModel.getAmount();

        if (minAmount != null) {
            amount.getEditText().setText(Integer.toString(minAmount), TextView.BufferType.EDITABLE);
            filterViewModel.setAmount(minAmount);
        }

        Integer maxPrice = filterViewModel.getPriceLimit();

        if (maxPrice != null) {
            priceLimit.getEditText().setText(Integer.toString(maxPrice), TextView.BufferType.EDITABLE);
            filterViewModel.setPriceLimit(maxPrice);
        }

        pickedCategories = filterViewModel.getSelectedCategories();

        if (pickedCategories.size() != 0) {
            categories.setText(String.join(", ", pickedCategories));
            filterViewModel.setSelectedCategories(pickedCategories.stream()
                    .map(name -> filterViewModel.getCategoriesList().getValue().stream().filter(c -> c.name.equals(name)).findFirst().get().id).collect(Collectors.toList()));
        }

        fromDate.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                long timestamp = LocalDate.of(year, month + 1, dayOfMonth).atTime(LocalTime.MIN).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
                filterViewModel.setFromDate(timestamp);
            }
        });

        toDate.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                long timestamp = LocalDate.of(year, month + 1, dayOfMonth).atTime(LocalTime.MAX).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
                filterViewModel.setToDate(timestamp);
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Navigation.findNavController(v).navigateUp();
            }
        });


        applyButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                filterViewModel.applyChanges();
                Navigation.findNavController(v).navigateUp();
            }
        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                filterViewModel.resetChanges();
                Navigation.findNavController(v).navigateUp();
            }
        });

        priceLimit.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    filterViewModel.setPriceLimit(Integer.parseInt(priceLimit.getEditText().getText().toString()));
                } catch (Exception e) {
                    filterViewModel.setPriceLimit(null);
                }
            }
        }));

        amount.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    filterViewModel.setAmount(Integer.parseInt(amount.getEditText().getText().toString()));
                } catch (Exception e) {
                    filterViewModel.setAmount(null);
                }
            }
        }));

        categories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);

                builder.setTitle("Select Categories");

                builder.setCancelable(false);

                builder.setMultiChoiceItems(categoriesArray, selectedCategories, new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i, boolean b) {
                        if (b) {
                            categoriesList.add(i);
                            Collections.sort(categoriesList);
                        } else {
                            categoriesList.remove(Integer.valueOf(i));
                        }
                    }
                });

                builder.setPositiveButton(R.string.ok_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        StringBuilder stringBuilder = new StringBuilder();
                        List<Integer> categoryIds = new ArrayList<Integer>();

                        for (int j = 0; j < categoriesList.size(); j++) {
                            String categoryName = categoriesArray[categoriesList.get(j)];
                            Integer categoryId = allCategories.stream()
                                    .filter(category -> category.name.equals(categoryName)).findFirst().get().id;
                            stringBuilder.append(categoryName);
                            categoryIds.add(categoryId);
                            if (j != categoriesList.size() - 1) {
                                stringBuilder.append(", ");
                            }
                        }
                        categories.setText(stringBuilder.toString());
                        filterViewModel.setSelectedCategories(categoryIds);
                    }
                });

                builder.setNegativeButton(R.string.cancel_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                builder.show();
            }
        });

        filterViewModel.getCategoriesList().observe(getViewLifecycleOwner(), new Observer<List<Category>>() {
            @Override
            public void onChanged(List<Category> categories) {
                allCategories = categories;
                categoriesArray = allCategories.stream()
                        .map(category -> category.name)
                        .collect(Collectors.toList()).toArray(new String[allCategories.size()]);

                if (selectedCategories == null || selectedCategories.length == 0) {
                    selectedCategories = new boolean[categoriesArray.length];
                }

                pickedCategories.forEach(category -> {
                    int index = allCategories.stream().map(c -> c.name).collect(Collectors.toList()).indexOf(category);
                    selectedCategories[index] = true;
                    categoriesList.add(index);
                });
            }
        });
        progressBar.setVisibility(View.GONE);
        return root;
    }
}