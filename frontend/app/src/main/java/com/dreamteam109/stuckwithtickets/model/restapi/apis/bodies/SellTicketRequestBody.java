package com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies;

public class SellTicketRequestBody {
    final String title;
    final int category;
    final String description;
    final Long eventTimestamp;
    final int amount;
    final int price;
    final String imageId;
    final double x;
    final double y;

    public SellTicketRequestBody(String title, int category, String description, Long eventTimestamp, int amount, int price, String imageId, double x, double y) {
        this.title = title;
        this.category = category;
        this.description = description;
        this.eventTimestamp = eventTimestamp;
        this.amount = amount;
        this.price = price;
        this.imageId = imageId;
        this.x = x;
        this.y = y;
    }
}
