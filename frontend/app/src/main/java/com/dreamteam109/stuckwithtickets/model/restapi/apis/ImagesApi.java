package com.dreamteam109.stuckwithtickets.model.restapi.apis;

import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.AddImageResponseBody;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ImagesApi {
    @Multipart
    @POST("images/add")
    Call<AddImageResponseBody> addImage(@Part MultipartBody.Part image);

}
