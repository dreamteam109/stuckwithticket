package com.dreamteam109.stuckwithtickets.ui.editprofile;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.dreamteam109.stuckwithtickets.R;
import com.dreamteam109.stuckwithtickets.StuckWithTicketsApplication;
import com.dreamteam109.stuckwithtickets.databinding.FragmentEditProfileBinding;
import com.dreamteam109.stuckwithtickets.model.Model;
import com.dreamteam109.stuckwithtickets.model.models.User;
import com.dreamteam109.stuckwithtickets.model.Validators;
import com.google.android.material.textfield.TextInputLayout;

import java.io.IOException;

public class EditProfileFragment extends Fragment {
    private static final int REQUEST_CAMERA = 1;
    private static final int PICK_IMAGE = 2;

    private EditProfileViewModel editProfileViewModel;
    private FragmentEditProfileBinding binding;

    TextInputLayout firstNameTil;
    TextInputLayout lastNameTil;
    TextInputLayout phoneNumberTil;

    ImageView imageImv;
    Button cameraBtn;
    Button clearImageBtn;
    Button galleryBtn;

    Button saveBtn;
    Button cancelBtn;
    ProgressBar progressBar;

    User user;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentEditProfileBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        firstNameTil = root.findViewById(R.id.edit_profile_first_name);
        lastNameTil = root.findViewById(R.id.edit_profile_last_name);
        phoneNumberTil = root.findViewById(R.id.edit_profile_phone);
        imageImv = root.findViewById(R.id.edit_profile_image_imv);
        cameraBtn = root.findViewById(R.id.edit_profile_camera_btn);
        clearImageBtn = root.findViewById(R.id.edit_profile_clear_image_btn);
        galleryBtn = root.findViewById(R.id.edit_profile_gallery_btn);
        saveBtn = root.findViewById(R.id.edit_profile_save_btn);
        cancelBtn = root.findViewById(R.id.edit_profile_cancel_btn);
        progressBar = root.findViewById(R.id.edit_profile_progressbar);

        saveBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                Log.d("StuckWithTicketsLog", "edit profile save click");
                if (!Validators.isNotEmptyString(editProfileViewModel.getFirstName().getValue())) {
                    Toast.makeText(getContext().getApplicationContext(), R.string.first_name_is_empty, Toast.LENGTH_LONG).show();
                } else if (!Validators.isNotEmptyString(editProfileViewModel.getLastName().getValue())) {
                    Toast.makeText(getContext().getApplicationContext(), R.string.last_name_is_empty, Toast.LENGTH_LONG).show();
                } else if (!Validators.isNotNull(editProfileViewModel.getPhoneNumber().getValue())) {
                    Toast.makeText(getContext().getApplicationContext(), R.string.phone_is_empty, Toast.LENGTH_LONG).show();
                } else {
                    editProfileViewModel.editProfile(new Model.EditProfileListener() {
                        @Override
                        public void onComplete() {
                            Navigation.findNavController(view).navigateUp();
                        }
                    });
                }
                progressBar.setVisibility(View.GONE);
            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                Log.d("TAG", "edit profile cancel click");
                Navigation.findNavController(v).navigateUp();
            }
        });

        firstNameTil.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editProfileViewModel.setFirstName(firstNameTil.getEditText().getText().toString());
            }
        }));

        lastNameTil.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editProfileViewModel.setLastName(lastNameTil.getEditText().getText().toString());
            }
        }));

        phoneNumberTil.getEditText().addTextChangedListener((new TextWatcher() {

            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                editProfileViewModel.setPhoneNumber(phoneNumberTil.getEditText().getText().toString());
            }
        }));

        cameraBtn.setOnClickListener(v -> {
            openSelectImageFromCamera();
        });

        clearImageBtn.setOnClickListener(v -> {
            clearImage();
        });

        galleryBtn.setOnClickListener(v -> {
            openSelectImageFromGallery();
        });
        progressBar.setVisibility(View.GONE);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        editProfileViewModel = new ViewModelProvider(this, new EditProfileViewModel.Factory(getActivity().getApplication()))
                .get(EditProfileViewModel.class);
        progressBar.setVisibility(View.VISIBLE);

        editProfileViewModel.getUser().observe(getViewLifecycleOwner(), user -> {
            if (user != null) {
                EditProfileFragment.this.user = user;

                firstNameTil.getEditText().setText(user.getFirstName());
                lastNameTil.getEditText().setText(user.getLastName());
                phoneNumberTil.getEditText().setText(user.getPhoneNumber());

                editProfileViewModel.setFirstName(user.getFirstName());
                editProfileViewModel.setLastName(user.getLastName());
                editProfileViewModel.setPhoneNumber(user.getPhoneNumber());

                Glide.with(EditProfileFragment.this)
                        .load(Model.instance.getFullImageUrl(user.getImageId()))
                        .fitCenter()
                        .placeholder(R.drawable.avatar)
                        .fallback(R.drawable.avatar)
                        .into(imageImv);
                progressBar.setVisibility(View.GONE);
            }
        });

        editProfileViewModel.getBitmap().observe(getViewLifecycleOwner(), new Observer<Bitmap>() {

            @Override
            public void onChanged(Bitmap bitmap) {
                progressBar.setVisibility(View.VISIBLE);
                Glide.with(EditProfileFragment.this)
                        .clear(imageImv);

                if (bitmap == null) {
                    imageImv.setImageResource(R.drawable.avatar);
                } else {
                    imageImv.setImageBitmap(bitmap);
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        progressBar.setVisibility(View.VISIBLE);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                editProfileViewModel.setBitmap((Bitmap) extras.get("data"));
            }
        } else if (requestCode == PICK_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(StuckWithTicketsApplication.getContext().getContentResolver(), data.getData());
                } catch (IOException e) {
                    Toast.makeText(getContext(), R.string.problem_getting_selected_image, Toast.LENGTH_LONG);
                }
                editProfileViewModel.setBitmap(bitmap);
            }
        }
        progressBar.setVisibility(View.GONE);
    }

    private void openSelectImageFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void openSelectImageFromGallery() {
        progressBar.setVisibility(View.VISIBLE);
        Intent intent = new Intent();
        intent.setType("image/png image/jpeg");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
        progressBar.setVisibility(View.GONE);
    }

    private void clearImage() {
        editProfileViewModel.setBitmap(null);
    }

}