package com.dreamteam109.stuckwithtickets.model.room;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.dreamteam109.stuckwithtickets.model.Converters;
import com.dreamteam109.stuckwithtickets.model.models.Ticket;

@TypeConverters({Converters.class})
@Database(entities = {Ticket.class}, version = 7)
public abstract class AppLocalDbRepository extends RoomDatabase {
    public abstract TicketDao ticketDao();
}
