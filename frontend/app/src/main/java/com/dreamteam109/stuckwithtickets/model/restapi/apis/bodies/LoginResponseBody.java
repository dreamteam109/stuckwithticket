package com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies;

public class LoginResponseBody {
    String token;

    public LoginResponseBody(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}
