package com.dreamteam109.stuckwithtickets;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.dreamteam109.stuckwithtickets.model.Model;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.appcompat.app.AppCompatActivity;

import com.dreamteam109.stuckwithtickets.databinding.ActivityMainBinding;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements PopupMenu.OnMenuItemClickListener {

    private static final String LOGO_CLICK_LISTENER_NAME = "NavBackToHome";

    private ActivityMainBinding binding;
    private final Map<String, View.OnClickListener> logoOnClickListeners = new HashMap<>();

    View customAppBar;
    ImageView appBarLogoImv;
    ImageButton appBarPersonImbtn;
    TextView appBarPersonLoginTv;
    ImageButton appBarPlusImbtn;

    private NavController navController;
    private final NavController.OnDestinationChangedListener onDestinationChangedListener = new NavController.OnDestinationChangedListener() {
        @Override
        public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
            Log.d("StuckWithTicketsLog", "MainActivity - nav destination id: " + destination.getId());
            Log.d("StuckWithTicketsLog", "MainActivity - nav destination label: " + destination.getLabel());

            showOrHideAppBarButtonsAccordingToState(destination.getId(), Model.instance.isLoggedIn().getValue());
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View root = binding.getRoot();
        setContentView(root);

        customAppBar = root.findViewById(R.id.custom_app_bar);
        appBarLogoImv = root.findViewById(R.id.app_bar_logo_imv);
        appBarPersonImbtn = root.findViewById(R.id.app_bar_person_imbtn);
        appBarPersonLoginTv = root.findViewById(R.id.app_bar_person_login_tv);
        appBarPlusImbtn = root.findViewById(R.id.app_bar_plus_imbtn);

        navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);

        Model.instance.isLoggedIn().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean isLoggedIn) {
                Log.d("StuckWithTicketsLog", "MainActivity - isLoggedIn: " + isLoggedIn);
                Log.d("StuckWithTicketsLog", "MainActivity - getCurrentDestination id: " + navController.getCurrentDestination().getId());
                showOrHideAppBarButtonsAccordingToState(navController.getCurrentDestination().getId(), isLoggedIn);
            }
        });

        // NOTE: This is needed to let the live data transformations work
        Model.instance.getCurrentUserId().observe(this, integer -> {
        });
        Model.instance.getCurrentUserEmail().observe(this, s -> {
        });

        appBarLogoImv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("StuckWithTicketsLog", "MainActivity - running all logo click listeners");
                logoOnClickListeners.values().forEach(listener -> listener.onClick(v));
            }
        });

        addLogoOnClickListener(LOGO_CLICK_LISTENER_NAME, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("StuckWithTicketsLog", "MainActivity - logo click listener");
                navigateBackToHomeFragment();
            }
        });

        appBarPersonImbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                personIconClickHandle();
            }
        });

        appBarPlusImbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.navController.navigate(R.id.action_global_add_ticket);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        this.navController.addOnDestinationChangedListener(onDestinationChangedListener);
    }

    @Override
    protected void onPause() {
        this.navController.removeOnDestinationChangedListener(onDestinationChangedListener);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        removeLogoOnClickListener(LOGO_CLICK_LISTENER_NAME);
    }

    private void showOrHideAppBarButtonsAccordingToState(@IdRes int destinationId, boolean isLoggedIn) {
        if (destinationId == R.id.nav_login || destinationId == R.id.nav_register) {
            hidePersonIconButton();
            hidePlusIconButton();
        } else {
            showPersonIconButton(isLoggedIn);
            if (!isLoggedIn || destinationId == R.id.nav_add_ticket) {
                hidePlusIconButton();
            } else {
                showPlusIconButton();
            }
        }
    }

    private void showPlusIconButton() {
        appBarPlusImbtn.setVisibility(View.VISIBLE);
    }

    private void hidePlusIconButton() {
        appBarPlusImbtn.setVisibility(View.GONE);
    }

    private void showPersonIconButton(boolean isLoggedIn) {
        appBarPersonImbtn.setVisibility(View.VISIBLE);
        appBarPersonLoginTv.setVisibility(!isLoggedIn ? View.VISIBLE : View.GONE);
    }

    private void hidePersonIconButton() {
        appBarPersonImbtn.setVisibility(View.GONE);
        appBarPersonLoginTv.setVisibility(View.GONE);
    }

    public void addLogoOnClickListener(String listenerName, View.OnClickListener listener) {
        logoOnClickListeners.put(listenerName, listener);
    }

    public void removeLogoOnClickListener(String listenerName) {
        logoOnClickListeners.remove(listenerName);
    }

    private void navigateBackToHomeFragment() {
        this.navController.popBackStack(R.id.nav_home, false);
    }

    private void personIconClickHandle() {
        if (Model.instance.isLoggedIn().getValue()) {
            showMenu(customAppBar);
        } else {
            this.navController.navigate(R.id.action_global_login);
        }
    }

    private void showMenu(View v) {
        PopupMenu popup = new PopupMenu(this, v);

        // This activity implements OnMenuItemClickListener
        popup.setOnMenuItemClickListener(this);
        popup.setGravity(Gravity.END);
        popup.inflate(R.menu.person_actions);
        Menu menu = popup.getMenu();
        menu.findItem(R.id.action_profile).setEnabled(navController.getCurrentDestination().getId() != R.id.nav_profile);
        menu.findItem(R.id.action_your_tickets).setEnabled(navController.getCurrentDestination().getId() != R.id.nav_your_tickets);
        popup.show();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_profile:
                this.navController.navigate(R.id.action_global_profile);
                return true;
            case R.id.action_your_tickets:
                this.navController.navigate(R.id.action_global_your_tickets);
                return true;
            case R.id.action_logout:
                new AlertDialog.Builder(MainActivity.this, R.style.AlertDialogTheme)
                        .setMessage(R.string.logout_dialog)
                        .setPositiveButton(R.string.yes, (dialog, which) -> {
                            Model.instance.logout();
                            navigateBackToHomeFragment();
                            Toast.makeText(getApplicationContext(), R.string.logged_out, Toast.LENGTH_LONG).show();
                        })
                        .setNegativeButton(R.string.no, (dialog, which) -> {
                        })
                        .show();
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        return this.navController.navigateUp()
                || super.onSupportNavigateUp();
    }

}
