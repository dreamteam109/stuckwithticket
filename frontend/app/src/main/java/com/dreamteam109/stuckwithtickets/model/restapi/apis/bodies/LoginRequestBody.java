package com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies;

public class LoginRequestBody {
    final String email;
    final String password;

    public LoginRequestBody(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
