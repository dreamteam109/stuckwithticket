package com.dreamteam109.stuckwithtickets.model.restapi.apis;

import com.dreamteam109.stuckwithtickets.model.models.User;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.EditProfileRequestBody;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.LoginRequestBody;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.LoginResponseBody;
import com.dreamteam109.stuckwithtickets.model.restapi.apis.bodies.RegisterRequestBody;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface UsersApi {
    @POST("users/register")
    Call<Void> register(@Body RegisterRequestBody requestBody);

    @POST("users/login")
    Call<LoginResponseBody> login(@Body LoginRequestBody requestBody);

    @GET("users/contactInfo/{id}")
    Call<User> getContactInfoById(@Path("id") int id);

    @GET("users/me")
    Call<User> getProfileInfo(@Header("Authorization") String header);

    @POST("users/edit")
    Call<Void> editProfile(@Header("Authorization") String header, @Body EditProfileRequestBody requestBody);

}
