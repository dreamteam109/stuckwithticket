package com.dreamteam109.stuckwithtickets.ui.register;

import androidx.annotation.Nullable;

/**
 * Data validation state of the register form.
 */
class RegisterFormState {
    @Nullable
    private Integer firstNameError;
    @Nullable
    private Integer lastNameError;
    @Nullable
    private Integer emailError;
    @Nullable
    private Integer passwordError;
    @Nullable
    private Integer verifyPasswordError;
    @Nullable
    private Integer phoneError;
    private boolean isDataValid;

    public RegisterFormState(@Nullable Integer firstNameError, @Nullable Integer lastNameError,
                             @Nullable Integer emailError, @Nullable Integer passwordError,
                             @Nullable Integer verifyPasswordError, @Nullable Integer phoneError) {
        this.firstNameError = firstNameError;
        this.lastNameError = lastNameError;
        this.emailError = emailError;
        this.passwordError = passwordError;
        this.verifyPasswordError = verifyPasswordError;
        this.phoneError = phoneError;
        this.isDataValid = false;
    }

    RegisterFormState(boolean isDataValid) {
        this.firstNameError = null;
        this.lastNameError = null;
        this.emailError = null;
        this.passwordError = null;
        this.verifyPasswordError = null;
        this.phoneError = null;
        this.isDataValid = isDataValid;
    }

    @Nullable
    Integer getFirstNameError() {
        return firstNameError;
    }

    @Nullable
    Integer getLastNameError() {
        return lastNameError;
    }

    @Nullable
    Integer getEmailError() {
        return emailError;
    }

    @Nullable
    Integer getPasswordError() {
        return passwordError;
    }

    @Nullable
    Integer getVerifyPasswordError() {
        return verifyPasswordError;
    }

    @Nullable
    Integer getPhoneError() {
        return phoneError;
    }

    boolean isDataValid() {
        return isDataValid;
    }
}