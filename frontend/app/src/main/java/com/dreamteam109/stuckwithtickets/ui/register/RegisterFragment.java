package com.dreamteam109.stuckwithtickets.ui.register;

import androidx.appcompat.content.res.AppCompatResources;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.dreamteam109.stuckwithtickets.R;
import com.dreamteam109.stuckwithtickets.StuckWithTicketsApplication;
import com.dreamteam109.stuckwithtickets.databinding.FragmentRegisterBinding;

import java.io.IOException;

public class RegisterFragment extends Fragment {
    private static final int REQUEST_CAMERA = 1;
    private static final int PICK_IMAGE = 2;

    private RegisterViewModel registerViewModel;
    private FragmentRegisterBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = FragmentRegisterBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerViewModel = new ViewModelProvider(this).get(RegisterViewModel.class);

        final EditText firstNameEditText = view.findViewById(R.id.register_first_name_et);
        final EditText lastNameEditText = view.findViewById(R.id.register_last_name_et);
        final EditText emailEditText = view.findViewById(R.id.register_email_et);
        final EditText passwordEditText = view.findViewById(R.id.register_password_et);
        final EditText verifyPasswordEditText = view.findViewById(R.id.register_verify_password_et);
        final EditText phoneEditText = view.findViewById(R.id.register_phone_et);
        final ImageView imageImv = view.findViewById(R.id.register_image_imv);
        final Button cameraBtn = view.findViewById(R.id.register_camera_btn);
        final Button clearImageBtn = view.findViewById(R.id.register_clear_image_btn);
        final Button galleryBtn = view.findViewById(R.id.register_gallery_btn);
        final ImageButton registerButton = view.findViewById(R.id.register_register_btn);
        final ProgressBar loadingProgressBar = view.findViewById(R.id.register_loading);

        registerViewModel.getRegisterFormState().observe(getViewLifecycleOwner(), new Observer<RegisterFormState>() {
            @Override
            public void onChanged(@Nullable RegisterFormState registerFormState) {
                if (registerFormState == null) {
                    return;
                }
                registerButton.setClickable(registerFormState.isDataValid());
                registerButton.setBackground(registerFormState.isDataValid() ?
                        AppCompatResources.getDrawable(getContext(), R.drawable.gradient_color_rounded) :
                        AppCompatResources.getDrawable(getContext(), R.drawable.gradient_color_rounded_gray)
                );
                if (registerFormState.getFirstNameError() != null) {
                    firstNameEditText.setError(getString(registerFormState.getFirstNameError()));
                }
                if (registerFormState.getLastNameError() != null) {
                    lastNameEditText.setError(getString(registerFormState.getLastNameError()));
                }
                if (registerFormState.getEmailError() != null) {
                    emailEditText.setError(getString(registerFormState.getEmailError()));
                }
                if (registerFormState.getPasswordError() != null) {
                    passwordEditText.setError(getString(registerFormState.getPasswordError()));
                }
                if (registerFormState.getVerifyPasswordError() != null) {
                    verifyPasswordEditText.setError(getString(registerFormState.getVerifyPasswordError()));
                }
                if (registerFormState.getPhoneError() != null) {
                    phoneEditText.setError(getString(registerFormState.getPhoneError()));
                }
            }
        });

        registerViewModel.getBitmap().observe(getViewLifecycleOwner(), new Observer<Bitmap>() {
            @Override
            public void onChanged(Bitmap bitmap) {
                if (bitmap == null) {
                    imageImv.setImageResource(R.drawable.avatar);
                } else {
                    imageImv.setImageBitmap(bitmap);
                }
            }
        });

        registerViewModel.getRegisterResult().observe(getViewLifecycleOwner(), new Observer<RegisterResult>() {
            @Override
            public void onChanged(@Nullable RegisterResult registerResult) {
                if (registerResult == null) {
                    return;
                }
                loadingProgressBar.setVisibility(View.GONE);
                if (registerResult.isSuccessful()) {
                    showRegisterSuccess(view);
                } else {
                    showRegisterFailed(registerResult.getErrorMessage());
                }
            }
        });

        TextWatcher afterTextChangedListener = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                registerViewModel.registerDataChanged(
                        firstNameEditText.getText().toString(),
                        lastNameEditText.getText().toString(),
                        emailEditText.getText().toString(),
                        passwordEditText.getText().toString(),
                        verifyPasswordEditText.getText().toString(),
                        phoneEditText.getText().toString()
                );
            }
        };
        firstNameEditText.addTextChangedListener(afterTextChangedListener);
        lastNameEditText.addTextChangedListener(afterTextChangedListener);
        emailEditText.addTextChangedListener(afterTextChangedListener);
        passwordEditText.addTextChangedListener(afterTextChangedListener);
        verifyPasswordEditText.addTextChangedListener(afterTextChangedListener);
        phoneEditText.addTextChangedListener(afterTextChangedListener);
        phoneEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loadingProgressBar.setVisibility(View.VISIBLE);
                    registerViewModel.register(
                            firstNameEditText.getText().toString(),
                            lastNameEditText.getText().toString(),
                            emailEditText.getText().toString(),
                            passwordEditText.getText().toString(),
                            phoneEditText.getText().toString()
                    );
                }
                return false;
            }
        });

        cameraBtn.setOnClickListener(v -> {
            openSelectImageFromCamera();
        });

        clearImageBtn.setOnClickListener(v -> {
            clearImage();
        });

        galleryBtn.setOnClickListener(v -> {
            openSelectImageFromGallery();
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingProgressBar.setVisibility(View.VISIBLE);
                registerViewModel.register(
                        firstNameEditText.getText().toString(),
                        lastNameEditText.getText().toString(),
                        emailEditText.getText().toString(),
                        passwordEditText.getText().toString(),
                        phoneEditText.getText().toString()
                );
            }
        });
        registerButton.setClickable(false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CAMERA) {
            if (resultCode == Activity.RESULT_OK) {
                Bundle extras = data.getExtras();
                registerViewModel.setBitmap((Bitmap) extras.get("data"));
            }
        } else if (requestCode == PICK_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Bitmap bitmap = null;
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(StuckWithTicketsApplication.getContext().getContentResolver(), data.getData());
                } catch (IOException e) {
                    Toast.makeText(getContext(), R.string.problem_getting_selected_image, Toast.LENGTH_LONG);
                }
                registerViewModel.setBitmap(bitmap);
            }
        }
    }

    private void openSelectImageFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void openSelectImageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/png image/jpeg");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    private void clearImage() {
        registerViewModel.setBitmap(null);
    }

    private void showRegisterSuccess(View view) {
        if (getContext() != null && getContext().getApplicationContext() != null) {
            Toast.makeText(getContext().getApplicationContext(),
                    R.string.registered_successfully, Toast.LENGTH_LONG).show();
        }
        Navigation.findNavController(view).navigateUp();
    }

    private void showRegisterFailed(String error) {
        if (getContext() != null && getContext().getApplicationContext() != null) {
            Toast.makeText(
                    getContext().getApplicationContext(),
                    error,
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

}