package com.dreamteam109.stuckwithtickets.ui.yourtickets;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.dreamteam109.stuckwithtickets.model.Model;
import com.dreamteam109.stuckwithtickets.model.models.Ticket;

import java.util.List;

public class YourTicketsViewModel extends ViewModel {
    private LiveData<List<Ticket>> myTickets = Transformations.distinctUntilChanged(Model.instance.getMyTickets());

    public LiveData<List<Ticket>> getTicketsList() {
        return myTickets;
    }
}