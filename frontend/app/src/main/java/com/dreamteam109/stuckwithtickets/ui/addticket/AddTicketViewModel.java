package com.dreamteam109.stuckwithtickets.ui.addticket;

import android.graphics.Bitmap;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.dreamteam109.stuckwithtickets.model.models.Category;
import com.dreamteam109.stuckwithtickets.model.Model;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AddTicketViewModel extends ViewModel {
    private MutableLiveData<String> title = new MutableLiveData<String>();
    private MutableLiveData<Integer> category = new MutableLiveData<Integer>();
    private MutableLiveData<String> description = new MutableLiveData<String>();
    private MutableLiveData<Long> date = new MutableLiveData<Long>();
    private MutableLiveData<Integer> amount = new MutableLiveData<Integer>();
    private MutableLiveData<Integer> price = new MutableLiveData<Integer>();
    private MutableLiveData<Bitmap> bitmap = new MutableLiveData<Bitmap>();
    private MutableLiveData<String> location = new MutableLiveData<String>();


    public MutableLiveData<String> getTitle() {
        return title;
    }

    public MutableLiveData<Integer> getCategory() {
        return category;
    }

    public MutableLiveData<String> getDescription() {
        return description;
    }

    public MutableLiveData<Long> getDate() {
        return date;
    }

    public MutableLiveData<Integer> getAmount() {
        return amount;
    }

    public MutableLiveData<Integer> getPrice() {
        return price;
    }

    public MutableLiveData<Bitmap> getBitmap() {
        return bitmap;
    }

    public MutableLiveData<String> getLocation() {
        return location;
    }


    public void setDate(Long date) {
        this.date.setValue(date);
    }

    public void setTitle(String title) {
        this.title.setValue(title);
    }

    public void setCategory(Integer category) {
        this.category.setValue(category);
    }

    public void setDescription(String description) {
        this.description.setValue(description);
    }

    public void setAmount(Integer amount) {
        this.amount.setValue(amount);
    }

    public void setPrice(Integer price) {
        this.price.setValue(price);
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap.setValue(bitmap);
    }

    public void setLocation(String location) {
        this.location.setValue(location);
    }

    public void sellTicket(int hour, int minute, double x, double y, Model.SellTicketListener listener) {

        Date date = new Date(this.date.getValue());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);

        if (bitmap.getValue() == null) {
            Model.instance.sellTicket(
                    title.getValue(),
                    category.getValue(),
                    description.getValue(),
                    calendar.getTimeInMillis(),
                    amount.getValue(),
                    price.getValue(),
                    null,
                    x,
                    y,
                    listener);
        } else {
            Model.instance.addImage(bitmap.getValue(), new Model.AddImageListener() {
                @Override
                public void onComplete(String imageId) {
                    Model.instance.sellTicket(
                            title.getValue(),
                            category.getValue(),
                            description.getValue(),
                            calendar.getTimeInMillis(),
                            amount.getValue(),
                            price.getValue(),
                            imageId,
                            x,
                            y,
                            listener);
                }
            });
        }
    }

    public LiveData<List<Category>> getCategoriesList() {
        return Model.instance.getAllCategories();
    }
}