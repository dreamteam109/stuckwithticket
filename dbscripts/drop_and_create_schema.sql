drop schema if exists stuckwithtickets cascade;

create schema stuckwithtickets;

-- =====

create table stuckwithtickets.images
(
    id            text                      not null
        constraint images_pk
            primary key,
    mime_type     text                      not null,
    image_data    bytea                     not null,
    creation_date timestamptz default now() not null
);

-- TODO: Add user interests later

create table stuckwithtickets.users
(
    id               serial                    not null
        constraint users_pk
            primary key,
    first_name       text                      not null,
    last_name        text                      not null,
    email            text                      not null,
    password         text                      not null,
    phone_number     text                      not null,
    image_id         text,
    is_deleted       boolean     default false not null,
    last_update_date timestamptz default now() not null,
    creation_date    timestamptz default now() not null
);

create unique index users_email_uindex
    on stuckwithtickets.users (email);

create table stuckwithtickets.categories
(
    id               serial                    not null
        constraint categories_pk
            primary key,
    name             text                      not null,
    is_deleted       boolean     default false not null,
    last_update_date timestamptz default now() not null,
    creation_date    timestamptz default now() not null
);

create table stuckwithtickets.tickets
(
    id               serial                    not null
        constraint tickets_pk
            primary key,
    user_id          integer                   not null
        constraint tickets_users_id_fk
            references stuckwithtickets.users (id),
    event_name       text                      not null,
    category_id      integer                   not null
        constraint tickets_categories_id_fk
            references stuckwithtickets.categories (id),
    description      text                      not null,
    event_datetime   timestamptz               not null,
    price            numeric                   not null,
    amount           integer                   not null,
    image_id         text,
    x      	         numeric                    not null,
    y   	         numeric                    not null,
    is_deleted       boolean     default false not null,
    last_update_date timestamptz default now() not null,
    creation_date    timestamptz default now() not null
);
